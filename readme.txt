This program is designed in a way that it will work even if the javascript has been disabled on the client browser (the 'help' button won't work obviously) or if the 'js/main.js' is not loaded.

The logic behind the scene is simple:
1. Grab the user input;
2. Break it down to individual lines;
3. Process each line, remove the '>' characters using a regular expression;
4. Put together the lines again;
5. Output the cleaned text to the front end.

The UI is simple. The user pastes the messed up email to the input box on the left/top, and clicks the 'Clean Up' button, then they will see the cleaned text on the right/bottom, which they will be able to select for copying to the clipboard. It is straightforward for the users to compare the differences between the before/after texts too.

I've tested this program enough times. It will only remove preceding '>' characters of each line, and won't accidentally remove any '>' characters in the email body.

Author: Chester Zhang
Date: 17/02/2017
