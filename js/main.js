$(function(){
  $('#myForm').on('submit', function(e){
    e.preventDefault();

    var lines = $('#messy_input').val().trim().split("\n");
    var newLines = [];
    $.each(lines, function(i, v){
      newLines.push(v.trim().replace(/^(>+\s*)+/, ""));
    });

    var cleanOutput = newLines.join("\n");
    $('#clean_output').val(cleanOutput);
  });
});
