<?php
  if (!empty($_POST["messy_input"])) {
    $lines = explode("\n", trim($_POST["messy_input"]));
    $new_lines = array();
    foreach ($lines as $line) {
      $new_lines[] = preg_replace("/^(>+\s*)+/", "", trim($line));
    }
    $clean_output = implode("\n", $new_lines);
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Chester's solution for Cortex assignment</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="text-center">
        <h1>Clean Up Forwarded Emails</h1>
        <button type="button" class="js-help-btn btn btn-info" data-toggle="modal" data-target="#helpModal">Help</button>
      </div>
      <form id="myForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
        <div class="row">
          <div class="col-sm-5">
            <div class="form-group">
              <label for="messy_input">Paste it here:</label>
              <textarea class="form-control" id="messy_input" name="messy_input" rows="20" autofocus><?php echo empty($_POST["messy_input"]) ? '' : $_POST["messy_input"]; ?></textarea>
            </div>
          </div>
          <div class="col-sm-2">
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block">Clean Up</button>
            </div>
          </div>
          <div class="col-sm-5">
            <div class="form-group">
              <label for="clean_output">Results:</label>
              <textarea class="form-control" id="clean_output" name="clean_output" rows="20" readonly><?php echo empty($clean_output) ? '' : $clean_output; ?></textarea>
            </div>
          </div>
        </div>
      </form>
    </div>

    <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="helpModalLabel">Instructions</h4>
          </div>
          <div class="modal-body">
            <ol>
              <li>Paste in your messy forwarded email to the left/top box</li>
              <li>Click the 'Clean Up' button in the middle</li>
              <li>Get the cleaned text in the right/bottom box</li>
              <li>Enjoy using this app</li>
            </ol>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>
